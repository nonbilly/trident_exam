<?php

class Model_Genre extends \Orm\Model
{
	protected static $_properties = array(
		
		'id',
		'name',
	);


	protected static $_table_name = 'genre';

}

