<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://welcomephp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
    
        public function get_list(){
        $data = array();
        $data['movie'] = Model_Movie::find('all');
        $data['genre'] = Model_Genre::find('all');
        return Response::forge(View::forge('welcome/list',$data));
        }
        
              
        public function post_list(){
           
        $image = Input::file('image');
        
        if($image['name'] !=""){
            //var_dump($image);
            move_uploaded_file($image['tmp_name'], 'assets/img/'.$image['name']);
            
            //DBへの保存
            $model = Model_Movie::forge();
            $model->image = 'assets/img/'.$image['name'];
            $model->name = Input::post('name');
            $model->genre_id = Input::post('genre_id');
            $model->star = Input::post('star');
            $model->save();
            
            Response::redirect('welcome/list');
        }else{
            Response::redirect('welcome/list');
        }
        }
        
        public function get_upload(){
        $data = array();
        return Response::forge(View::forge('welcome/file',$data));
        }
        
        
        public function post_upload(){
           
        $image = Input::file('image');
        $name = Input::post('name');
        $genre_id = Input::post('genre_id');
        $star = Input::post('star');
        
        
        if($image['name'] !=""){
            //var_dump($image);
            move_uploaded_file($image['tmp_name'], 'assets/img/'.$image['name']);
            
            //DBへの保存
            $model = Model_Movie::forge();
            $model->image = 'assets/img/'.$image['name'];
            $model->name = 'assets/name/'.$name['name'];
            $model->genre_id = 'assets/genre_id/'.$genre_id['name'];
            $model->star = 'assets/star/'.$star['name'];
            $model->save();
            
            Response::redirect('welcome/list');
        }else{
            Response::redirect('welcome/upload');
        }
          }
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
