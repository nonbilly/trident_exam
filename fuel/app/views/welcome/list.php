一覧表示
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>movie collection</title>
    </head>
    <style>
    .l-left{
        float: left;
        } 
        
    .clear{
        clear: both;    
        }
        
    .rate {
        position: relative;
        display: inline-block;
        width: 100px;
        height: 20px;
        font-size: 20px;
        }
    .rate:before, .rate:after {
        position: absolute;
        top: 0;
        left: 0;
        content: '★★★★★';
        display: inline-block;
        height: 20px;
        line-height: 20px;
        }
    .rate:before {
        color: #ddd; /*星色*/
        }
    .rate:after {
        color: #ffa500;
        overflow: hidden;
        white-space: nowrap; /*重要*/
        }

    .rate00:after{width: 0;}
    .rate05:after{width: 10px;}
    .rate10:after{width: 20px;}
    .rate15:after{width: 30px;}
    .rate20:after{width: 40px;}
    .rate25:after{width: 50px;}
    .rate30:after{width: 60px;}
    .rate35:after{width: 70px;}
    .rate40:after{width: 80px;}
    .rate45:after{width: 90px;}
    .rate50:after{width: 100px;}
    </style>
    <body>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="file_image l-left">
          画像：<input type="file" name="image">
        </div>
        <div class="title_name clear l-left">
          タイトル：<input class="title" type="text" name="name">
        </div>
        <div class="genre_genreid clear l-left">
          ジャンル：
          <select name="genre_id">
            <?php foreach ($genre as $g) : ?>
              <option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
            <?php endforeach; ?>
          </select>  
        </div>
        <div class="rating_star clear l-left">
          評価：<input class="rating" type="text" name="star">
        </div>
          <input class="l-left clear" type="submit" value="送信">
    </form>
    <table class="l-left clear" border="1">
        <tr>
            <th>ID</th>
            <th>画像</th>
            <th>タイトル</th>
            <th>ジャンル</th>
            <th>評価<span class="rate rate00"></span></th>
        </tr>
        <?php foreach ($movie as $mv) : ?>
        <tr>
            <td><?php echo $mv['id']; ?></td>
            <td><img src="<?php echo Uri::base(). $mv['image']?>" width="300"></td>
            <td><?php echo $mv['name']; ?></td>
            <td><?php echo $genre[ $mv['genre_id'] ]['name']; ?></td>
            <td><?php echo $mv['star']; ?></td>
            <td>
                <!--<form action="<?php echo Uri::create('fruits'); ?>" method="post">
                    <input type="hidden" name="update_id" value="<?= $mv['id']?>">
                    <input type="submit" name="update" value="編集">
                </form>
                <form action="<?php echo Uri::create('fruits/delete'); ?>" method="post">
                    <input type="hidden" name="id" value="<?= $mv['id']?>">
                    <input type="submit" name="delete" value="削除" onclick="if (confirm('削除してもよろしいですか？')){return true;}else{return false;}">
                </form>-->
            </td>
        </tr>
        <?php endforeach; ?>
    </table>

    </body>
</html>