<?php

class Model_Movie extends \Orm\Model
{
	protected static $_properties = array(
		
		'id',
		'image',
		'name',
		'genre_id',
		'star',
	);


	protected static $_table_name = 'items';

}

